import csv
import tempfile
from typing import Optional

import uvicorn
from fastapi import FastAPI
from fastapi.responses import FileResponse
from pony.orm import select, db_session

from models import db, Product

app = FastAPI()

db.bind(provider='postgres', user='postgres', password='', host='', database='web_scraping')
db.generate_mapping(create_tables=True)

# @app.get('/assortment')
# async def assortment():
#     with orm.db_session:
#        stores = db.Store.select()
#        import ipdb; ipdb.set_trace()
#        result = [PersonInDB.from_orm(p) for p in stores]
#     return result


# @app.get('/seller')
# async def seller(sku: Optional[str] = None):
#     with orm.db_session:
#        stores = db.Store.select()
#        import ipdb; ipdb.set_trace()
#        result = [StoreInDB.from_orm(p) for p in stores]
#     return result

def CsvResponse(rows, name='report.csv', delimiter='|'):
    response = tempfile.NamedTemporaryFile(mode='w', delete=False, suffix='.csv')
    writer = csv.writer(response, delimiter=delimiter)
    for row in rows:
        row = [i for i in row]
        writer.writerow(row)

    return FileResponse(path=response.name, media_type='text/csv', filename=name)


@app.get("/seller")
async def seller(sku: Optional[str] = None, id_department: Optional[str] = None):
    rows = [['name', 'sku', 'department', 'category', 'seller_store', 'seller_player', 'price_store', 'price_player' , 'discount_store', 'avaliable', 'stock_qty', 'url', 'image', 'created_at', 'hour']]
    with db_session:
        products = select(c for c in Product)
        if sku:
            products = products.filter(lambda p: p.sku == sku)
        if id_department:
            products = products.filter(lambda p: p.category.department.code == id_department)

        for product in products:
            for seller in product.sellers_players:
                row = []
                row.append(product.name)
                row.append(product.sku)
                row.append(product.category.department.name)
                row.append(product.category.name)
                row.append(product.store.name)
                row.append(seller.store.name)
                row.append(product.price_to)
                row.append(seller.price_to)
                row.append(product.discount)
                row.append('S' if product.available else 'N')
                row.append(product.stock_qty)
                row.append(f'https://programada.shopper.com.br/shop/destaques/{product.url}')
                row.append(product.image)
                row.append(product.created_at)
                row.append(product.hour.strftime('%H:%M:%S'))
                rows.append(row)

    return CsvResponse(rows, name='seller.csv')


@app.get("/assortment")
async def seller(sku: Optional[str] = None, id_department: Optional[str] = None):
    rows = [['name', 'sku', 'department', 'category', 'url', 'image', 'price_to', 'discount', 'avaliable', 'stock_qty', 'store', 'created_at', 'hour']]
    with db_session:
        products = select(c for c in Product)
        if sku:
            products = products.filter(lambda p: p.sku == sku)
        if id_department:
            products = products.filter(lambda p: p.category.department.code == id_department)

        for product in products:
            row = []
            row.append(product.name)
            row.append(product.sku)
            row.append(product.category.department.name)
            row.append(product.category.name)
            row.append(f'https://programada.shopper.com.br/shop/destaques/{product.url}')
            row.append(product.image)
            row.append(product.price_to)
            row.append(product.discount)
            row.append('S' if product.available else 'N')
            row.append(product.stock_qty)
            row.append(product.store.name)
            row.append(product.created_at)
            row.append(product.hour.strftime('%H:%M:%S'))
            rows.append(row)

    return CsvResponse(rows, name='assortment.csv')

if __name__ == "__main__":
    uvicorn.run(app)