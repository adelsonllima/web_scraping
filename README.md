# README #

Projeto que faz scrap do https://landing.shopper.com.br/

### Configurar o projeto ###

* Clonar o projeto
```shell
git clone https://adelsonllima@bitbucket.org/adelsonllima/web_scraping.git
```
* Copiar o arquivo .env.sample para .env e configurar as variáveis
```shell
cp .env.sample .env
```
* Executar o pip install no arquivo requirements.txt  (pip install -r requirementes.txt)
```shell
pip install -r requirementes.txt
```
* Executar o script python que faz o scrap dos produtos (python scrapy.py), opcionalmente você pode passar como parâmetro o id do departamento para executar o scrap somente dele
```shell
python scrap.py
## ou
python scrap.py 22 #id do departamento Alimentos
```
* Executar script que inicia o servidor da api
```shell
python api.py
```
* Acessar o endereço http://localhost:8000/docs que será exibido um endpoint para baixar os relatório, podendo ser filtrado po sku e pelo id do departamento, conforme documentação.