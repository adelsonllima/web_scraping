fastapi==0.73.0
pony==0.7.14
psycopg2==2.9.3
pydantic==1.9.0
uvicorn==0.17.0.post1
beautifulsoup4==4.10.0
requests==2.27.1
python-dotenv==0.19.2