from datetime import time, date, datetime
from pony import orm
from pony.orm import composite_key

orm.set_sql_debug(True)
db = orm.Database()


class Store(db.Entity):
    name = orm.Required(str, unique=True)
    products = orm.Set('Product')
    products_sellers_players = orm.Set('ProductSeller')


class Department(db.Entity):
    code = orm.Required(int, unique=True)
    name = orm.Required(str, unique=True)
    url = orm.Required(str, unique=True)
    icon = orm.Required(str)
    categories = orm.Set('Category')


class Category(db.Entity):
    code = orm.Required(int, unique=True)
    name = orm.Required(str, unique=True)
    url = orm.Required(str, unique=True)
    department = orm.Required(Department)
    products = orm.Set('Product')


class Product(db.Entity):
    code = orm.Required(int, unique=True)
    name = orm.Required(str, unique=True)
    sku = orm.Required(str, unique=True)
    category = orm.Required(Category)
    url = orm.Required(str)
    image = orm.Required(str)
    store = orm.Required(Store)
    price_to = orm.Required(float)
    discount = orm.Required(str)
    available = orm.Required(bool)
    stock_qty = orm.Required(int)
    created_at = orm.Required(date)
    hour = orm.Required(time)
    sellers_players = orm.Set('ProductSeller')


class ProductSeller(db.Entity):
    product = orm.Required(Product)
    store = orm.Required(Store)
    price_to = orm.Required(float)
    last_update = orm.Required(datetime)
    composite_key(product, store, last_update)
