# -*- coding: utf-8 -*-
import os
import sys
from datetime import datetime

import requests
from dotenv import load_dotenv
from pony.orm import db_session

from models import Department, Category, Product, Store, db, ProductSeller

load_dotenv()

DATABASE_PROVIDER = os.environ.get('DATABASE_PROVIDER', 'postgres')
DATABASE_NAME = os.environ.get('DATABASE_NAME', 'web_scraping')
DATABASE_USER = os.environ.get('DATABASE_USER', 'postgres')
DATABASE_PASSWORD = os.environ.get('DATABASE_PASSWORD', '')
DATABASE_HOST = os.environ.get('DATABASE_HOST', '')

db.bind(provider=DATABASE_PROVIDER, user=DATABASE_USER, password=DATABASE_PASSWORD, host=DATABASE_HOST,
        database=DATABASE_NAME)
db.generate_mapping(create_tables=True)

BEARER_TOKEN = os.environ.get('BEARER_TOKEN')

headers = {"Authorization": os.environ.get('BEARER_TOKEN')}


def get_or_create(Class, **kwargs):
    with db_session:
        object = Class.get(**kwargs)
        if not object:
            object = Class(**kwargs)

    return object


def parse_float(value):
    value = value.split()[1]
    return value.replace('.', '').replace(',', '.')


def parse_datetime(value):
    return datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%fz")


class DepartmentSpider:
    URL = 'https://siteapi.shopper.com.br/catalog/departments'

    def parse(self, shopper_store, id_department):
        response = requests.get(self.URL, headers=headers).json()
        for department_data in response['departments']:
            with db_session:
                code = department_data['id']
                if id_department and id_department != code:
                    continue
                department = Department.get(code=code)
                if not department:
                    department = Department(
                        code=code,
                        name=department_data['name'],
                        url=department_data['url'],
                        icon=department_data['icon'],
                    )
            CategorySpider().parse(shopper_store, department)
            # for subdepartment_data in department_data['subdepartments']:
            #     category = Category(
            #         code=subdepartment_data['id'],
            #         name=subdepartment_data['name'],
            #         url=subdepartment_data['url'],
            #         department=department,
            #     )


class CategorySpider:
    URL = 'https://siteapi.shopper.com.br/catalog/departments/{}'

    def parse(self, shopper_store, department):
        response = requests.get(self.URL.format(department.code), headers=headers).json()
        for subdepartment_data in response['subdepartments']:
            with db_session:
                code = subdepartment_data['id']
                category = Category.get(code=code)
                if not category:
                    department = Department[department.id]
                    category = Category(
                        code=code,
                        name=subdepartment_data['name'],
                        url=subdepartment_data['url'],
                        department=department,
                    )
            ProdutosSpider().parse(shopper_store, department, category)


class ProdutosSpider:
    URL = 'https://siteapi.shopper.com.br/catalog/products?department={}&subdepartment={}&page={}&size=20&'

    def parse(self, shopper_store, department, category):
        pagina = 1
        url = self.URL.format(department.code, category.code, pagina)
        response = requests.get(url, headers=headers).json()
        while not response['last']:
            for product_data in response['products']:
                now = datetime.now()
                with db_session:
                    code = product_data['id']
                    product = Product.get(code=code)
                    if not product:
                        category = Category[category.id]
                        shopper_store = Store[shopper_store.id]
                        product = Product(
                            code=code,
                            name=product_data['name'],
                            sku=f'{code}',
                            category=category,
                            url=product_data['url'],
                            image=product_data['image'],
                            store=shopper_store,
                            price_to=parse_float(product_data['price']),
                            discount=product_data['savingPercentage'] or '-',
                            available=product_data['maxCartQuantity'] > 0,
                            stock_qty=product_data['maxCartQuantity'],
                            created_at=now.date(),
                            hour=now.time(),
                        )
                    for merchant_data in product_data['merchants']:
                        last_update = parse_datetime(merchant_data['lastUpdate'])
                        price_to = parse_float(merchant_data['price'])
                        store = get_or_create(Store, **{'name': merchant_data['name']})
                        product_store = get_or_create(ProductSeller, **{'product': product,
                                                                        'store': store,
                                                                        'last_update': last_update,
                                                                        'price_to': price_to})

            pagina += 1
            url = self.URL.format(department.code, category.code, pagina)
            response = requests.get(url, headers=headers).json()


shopper_store = get_or_create(Store, **{'name': 'Shopper'})

id_department = None
if len(sys.argv) == 2:
    id_department = int(sys.argv[1])

DepartmentSpider().parse(shopper_store, id_department)
